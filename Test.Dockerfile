FROM microsoft/dotnet:2.2-sdk
WORKDIR /app

RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs

COPY ./my-app.Test ./my-app.Test/
COPY ./my-app ./my-app/
COPY ./PipelinePracticeApp.sln ./

ENTRYPOINT ["dotnet", "test"]