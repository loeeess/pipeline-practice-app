FROM microsoft/dotnet:2.2-sdk
WORKDIR /app

RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs

COPY ./my-app/*.csproj ./
RUN dotnet restore 

COPY ./my-app ./

ENTRYPOINT ["dotnet", "run"]